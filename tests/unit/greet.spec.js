import { shallowMount } from '@vue/test-utils';
import Greet from '@/components/Greet.vue';
// import { state } from '@/store/modules/greet';


describe('Greet.vue', () => {
  // it('has a created hook', () => {
  //   expect(typeof Greet.created).toBe('function');
  // });
  it('renders props.msg when passed', () => {
    const msg = 'new message';
    const wrapper = shallowMount(Greet, {
      propsData: { msg },
    });
    expect(wrapper.text()).toMatch(msg);
  });
  it('Requirment 1', () => {
    const wrapper = shallowMount(Greet);
    const greet = 'hello Yoongi';
    wrapper.vm.greet_name = 'Yoongi';
    wrapper.vm.greet();
    expect(wrapper.vm.greetings).toEqual(greet);
  });
  it('Requirment 2', () => {
    const wrapper = shallowMount(Greet);
    const greet = 'hello my friend';
    wrapper.vm.greet_name = '';
    wrapper.vm.greet();
    expect(wrapper.vm.greetings).toEqual(greet);
  });
  it('Requirment 3', () => {
    const wrapper = shallowMount(Greet);
    const greet = 'HELLO YOONGI';
    wrapper.vm.greet_name = 'YOONGI';
    wrapper.vm.greet();
    expect(wrapper.vm.greetings).toEqual(greet);
  });
  it('Requirment 4', () => {
    const wrapper = shallowMount(Greet);
    const greet = 'hello yoongi and jhope';
    wrapper.vm.greet_name = 'yoongi, jhope';
    wrapper.vm.greet();
    expect(wrapper.vm.greetings).toEqual(greet);
  });
  it('Requirment 5', () => {
    const wrapper = shallowMount(Greet);
    const greet = 'hello yoongi, jhope and jungkook';
    wrapper.vm.greet_name = 'yoongi, jhope, jungkook';
    wrapper.vm.greet();
    expect(wrapper.vm.greetings).toEqual(greet);
  });
  it('Requirment 6', () => {
    const wrapper = shallowMount(Greet);
    const greet = 'hello yoongi and jungkook. AND HELLO JHOPE';
    wrapper.vm.greet_name = 'yoongi, JHOPE, jungkook';
    wrapper.vm.greet();
    expect(wrapper.vm.greetings).toEqual(greet);
  });
  it('Requirment 6.2', () => {
    const wrapper = shallowMount(Greet);
    const greet = 'hello jhope and jungkook. AND HELLO YOONGI';
    wrapper.vm.greet_name = 'YOONGI, jhope, jungkook';
    wrapper.vm.greet();
    expect(wrapper.vm.greetings).toEqual(greet);
  });
  it('Requirment 6.3', () => {
    const wrapper = shallowMount(Greet);
    const greet = 'hello yoongi and jhope. AND HELLO JUNGKOOK';
    wrapper.vm.greet_name = 'yoongi, jhope, JUNGKOOK';
    wrapper.vm.greet();
    expect(wrapper.vm.greetings).toEqual(greet);
  });
});
