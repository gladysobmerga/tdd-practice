import Vue from 'vue';
import Vuex from 'vuex';

import greet from '@/store/modules/greet';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
  },
  mutations: {
  },
  actions: {
  },
  modules: {
    greet,
  },
});
