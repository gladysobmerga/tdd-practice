const data = {
  greet_name: '',
};

export const getters = {
  greet_name: state => state.greet_name,
};

export default {
  state: data,
  getters,
};
